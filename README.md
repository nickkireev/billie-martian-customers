## ReactJS, Hooks, Redux, Material-UI, create-react-app

Billie.io 

### Case Study Mission 2121

## **Task**
## PART 1 
### Display a list of companies
Create a simple (React) SPA that displays a list of Martian customers of our service. 
The list should display the following fields:
* Company Name
* Date of a first purchase
* Total budget
* Budget spent
* Budget left

The budgets must be displayed in the german 
format, with 2 decimals.
You don’t need to implement the fetching logic or any server; for the company data, feel free to use 
the following mock:
```javascript
[{
 id: 1,
 name: "Martian Firma",
 budget: 10000.0000,
 budget_spent: 4500.0000,
 date_of_first_purchase: "2119-07-07",
}, {
 id: 2,
 name: "Solar Firma",
 budget: 1123.2200,
 budget_spent: 451.3754,
 date_of_first_purchase: "2120-01-14",
}, {
 id: 42,
 name: "Yellow Corp.",
 budget: 1000000.0000,
 budget_spent: 1000.0000,
 date_of_first_purchase: "2121-12-24",
},
]
```

## PART 2
### Allow to edit the total budget

Clicking the company row should open a modal 
with the company’s name and one input field:
* The field is pre-filled with the total budget for 
the that company; 
* The user should be able to edit the value and 
submit;
If the value is not valid, no submission should 
be possible;
* The new total budget for this company should 
be updated in the companies list.
Write unit test descriptions for the modal 
component (implementation is a nice to have).

**Notes:**
* On the frontend side, all the numeric values must have two decimals, and be displayed in the german locale
* It is up to you to use create-react-app;
* It is possible to use redux
* It is up to you to use any CSS framework, or no framework at all 

**Bonus:**
* The new value should not be less than the budget 
spent;
* After successful submission of the new total budget, 
a success message should be displayed.
* Provide the actual implementation of the test cases 
previously defined.

### Spent time

4 hours
 
### Run project 

```sh
npm ci
```
```sh
npm start
```
### Run tests

```sh
npm run test
```
### Developer
[@nickkireev]

[@nickkireev]: <https://bitbucket.org/nickkireev>