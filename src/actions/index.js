export const fetchCompaniesSuccess = data => ({
    type: 'FETCH_COMPANIES_SUCCESS',
    data
});

export const updateCompany = (dispatch, data) => {
    return new Promise((resolve, reject) => {
        if (data && data.id) {
            dispatch(updateCompanySuccess(data));
            resolve();
        } else {
            const error = 'Data is wrong';
            dispatch(updateCompanyFailure(error));
            reject(error);
        }
    });
}

export const updateCompanySuccess = data => ({
    type: 'UPDATE_COMPANY_SUCCESS',
    data
});

export const updateCompanyFailure = error => ({
    type: 'UPDATE_COMPANY_FAIL',
    error
});

export const addSuccessHint = text => addHint({ severity: 'success', text });

export const addErrorHint = text => addHint({ severity: 'error', text });

// export const addWarningHint = text => addHint({ severity: 'warning', text });

// export const addInfoHint = text => addHint({ severity: 'info', text });

export const addHint = ({ severity, text }) => ({
    type: 'ADD_HINT',
    severity,
    text
});

export const removeHint = (id) => ({
    type: 'REMOVE_HINT',
    id
});
