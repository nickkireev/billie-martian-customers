import React, {
    useState,
    useEffect,
} from 'react';
import { useSelector, useDispatch } from "react-redux";

import { createSelector } from 'reselect'
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import FormattedInput from '../NumberFormatInput/index';
import { updateCompany } from '../../actions'
import { addSuccessHint, addErrorHint } from './../../actions/index';

export const TOTAL_BUDGET_ERROR_MSG = "Total Budget can't be less than Spent Budget";

const useStyles = makeStyles((theme) => ({
    form: {
        display: 'flex',
        flexDirection: 'column',
    },
    formControl: {
        minWidth: 120,
        margin: theme.spacing(1),
    },
}));

export default function Company(props) {
    const classes = useStyles();
    const dispatch = useDispatch();

    const [open, setOpen] = useState(false);
    const [error, setError] = useState(false);
    const [helper, setHelper] = useState('');
    const [newBudget, setNewBudget] = useState(0);

    let findCompanyById = createSelector(
        (state) => state.companies,
        (companies) => companies.filter(row => row.id === props.selectedRowId)[0]
    );

    const data = useSelector(findCompanyById);

    let selectedDataById = {
        name: '',
        budget: 0,
        budget_spent: 0,
    }

    if (data) {
        selectedDataById = data;
    }

    const {
        name,
        budget,
        budget_spent,
    } = selectedDataById;

    useEffect(() => {
        setOpen(props.open);
        setNewBudget(budget);
    }, [props.open, budget]);

    const handleClose = () => {
        setOpen(false);
        setNewBudget(0);
        props.handleCloseModal();
    };

    const handleSubmit = () => {
        if (error) {
            return;
        }
        let data = {
            ...selectedDataById,
            budget: Number.parseFloat(newBudget)
        };

        updateCompany(dispatch, data)
            .then(
                () => dispatch(addSuccessHint('Update successfull')),
                (error) => dispatch(addErrorHint(error)),
            );
        handleClose();
    };

    return (
        <Dialog
            fullWidth={true}
            open={open}
            onClose={handleClose}
            aria-labelledby="max-width-dialog-title"
        >
            <DialogTitle id="max-width-dialog-title">Company Info</DialogTitle>
            <DialogContent>
                <form className={classes.form} noValidate>
                    <FormControl className={classes.formControl}>
                        <TextField
                            id="company-name"
                            inputProps={{ readOnly: true, }}
                            label="Company Name"
                            value={name}
                            fullWidth
                            variant="outlined"
                        />
                    </FormControl>
                    <FormControl className={classes.formControl}>
                        <FormattedInput
                            id="total-budget"
                            label="Total Budget"
                            value={newBudget}
                            fullWidth
                            onChange={({ target }) => {
                                setNewBudget(target.value);
                                setError(target.value < budget_spent);
                                setHelper(target.value < budget_spent ? TOTAL_BUDGET_ERROR_MSG : '')
                            }}
                            error={error}
                            variant="outlined"
                            helperText={helper}
                        />
                    </FormControl>
                    <FormControl className={classes.formControl}>
                        <FormattedInput
                            id="spent-budget"
                            inputProps={{ readOnly: true, }}
                            label="Spent Budget"
                            value={budget_spent}
                            fullWidth
                            variant="outlined"
                        />
                    </FormControl>
                </form>
            </DialogContent>
            <DialogActions>
                <Button id="company-edit-submit" onClick={handleSubmit} disabled={!!error} color="primary" variant="contained">
                    Submit
                </Button>
                <Button id="company-edit-close" onClick={handleClose} color="primary">
                    Close
                </Button>
            </DialogActions>
        </Dialog>
    );
}
