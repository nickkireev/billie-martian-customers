import React from 'react'
// fireEvent and screen impoted from @testing-library/react
// render - custom with Provider and so on
import { render, fireEvent, screen } from '../../utils/test-utils';
import EditCompanyModule, { TOTAL_BUDGET_ERROR_MSG } from './index';

test('EditCompanyModule can be rendered and closed', async () => {
    // mocked function
    const handleClose = jest.fn();

    render(<EditCompanyModule open={true} selectedDataById={'1'} handleCloseModal={handleClose} />)

    // Act
    expect(screen.getByLabelText(/Company Name/i)).toBeInTheDocument();
    expect(screen.getByLabelText(/Total Budget/i)).toBeInTheDocument();
    expect(screen.getByLabelText(/Spent Budget/i)).toBeInTheDocument();

    // error is not present
    let rg = new RegExp(TOTAL_BUDGET_ERROR_MSG, "i");
    expect(screen.queryByText(rg)).not.toBeInTheDocument();

    // click on close button
    fireEvent.click(screen.getByRole('button', { name: /Close/i }));
    expect(handleClose).toHaveBeenCalledTimes(1)
});
