import React, {
    useState,
    useEffect,
} from 'react';
import { useSelector, useDispatch } from "react-redux";

import NumberFormat from 'react-number-format';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { fetchCompaniesSuccess } from '../../actions'
import EditCompanyModule from '../EditCompanyModule/index';
import CommonHints from '../CommonHints';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

export default function MainTable() {
    const classes = useStyles();
    const data = useSelector(state => state.companies);
    const dispatch = useDispatch();

    useEffect(() => {
        const fetchData = () => {
            const mockedData =
                [
                    {
                        id: 1,
                        name: "Martian Firma",
                        budget: 10000.0000,
                        budget_spent: 4500.0000,
                        date_of_first_purchase: "2119-07-07",
                    },
                    {
                        id: 2,
                        name: "Solar Firma",
                        budget: 1123.2200,
                        budget_spent: 451.3754,
                        date_of_first_purchase: "2120-01-14",
                    },
                    {
                        id: 42,
                        name: "Yellow Corp.",
                        budget: 1000000.0000,
                        budget_spent: 1000.0000,
                        date_of_first_purchase: "2121-12-24",
                    },
                ];

            dispatch(fetchCompaniesSuccess(mockedData))
        };

        fetchData();
    }, [dispatch]);

    const numFormat = (num) => (<NumberFormat
        value={num}
        displayType={'text'}
        thousandSeparator={true}
        renderText={(value, props) => <div {...props}>{value}</div>}
        allowNegative={false}
        decimalSeparator="."
        decimalScale={2}
        fixedDecimalScale={true}
        isNumericString
    />);
    const dateFormat = (date) => {
        const formattedDateFunc = (d = new Date()) => {
            return [d.getDate(), d.getMonth() + 1, d.getFullYear()]
                .map(n => n < 10 ? `0${n}` : `${n}`).join('.');
        }
        const formatedDate = formattedDateFunc(new Date(date));

        return (
            <NumberFormat value={formatedDate}
                format="##.##.####"
                placeholder="DD.MM.YYYY"
                mask={['D', 'D', 'M', 'M', 'Y', 'Y', 'Y', 'Y']}
                displayType={'text'}
                renderText={(value, props) => <div {...props}>{value}</div>}
            />
        );
    }

    const [isEditOpen, setIsEditOpen] = useState(false);
    const [selectedRowId, setSelectedRowId] = useState(null);

    const handleOpenModal = (id) => {
        setIsEditOpen(true);
        setSelectedRowId(id);
    }

    const handleCloseModal = () => {
        setIsEditOpen(false);
        setSelectedRowId(null);
    }

    return (
        <>
            <CommonHints />
            <TableContainer component={Paper}>
                <Table className={classes.table} size="small" aria-label="a dense table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Company Name</TableCell>
                            <TableCell align="right">Date of a first purchase</TableCell>
                            <TableCell align="right">Total budget</TableCell>
                            <TableCell align="right">Budget spent</TableCell>
                            <TableCell align="right">Budget left</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.map((row) => (
                            <TableRow key={row.name} hover onClick={() => handleOpenModal(row.id)}>
                                <TableCell component="th" scope="row">{row.name}</TableCell>
                                <TableCell align="right">{dateFormat(row.date_of_first_purchase)}</TableCell>
                                <TableCell align="right">{numFormat(row.budget)}</TableCell>
                                <TableCell align="right">{numFormat(row.budget_spent)}</TableCell>
                                <TableCell align="right">{numFormat(row.budget - row.budget_spent)}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            {isEditOpen && <EditCompanyModule selectedRowId={selectedRowId} open={isEditOpen} handleCloseModal={() => handleCloseModal()} />}
        </>
    );
}
