
import { useSelector } from 'react-redux';

import Alert from '@material-ui/lab/Alert';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close';
import { useDispatch } from 'react-redux';
import { removeHint } from '../../actions';

export default function CommonHints() {
    const hints = useSelector(state => state.hints);

    return (hints.map(hint => <CommonHint key={hint.id} {...hint} />));
}

function CommonHint({ id = null, severity = 'info', text = 'Info', open = false }) {
    const dispatch = useDispatch();

    if (id === null) {
        return (<></>);
    }

    return (
        <Collapse in={open}>
            <Alert
                severity={severity}
                action={
                    <IconButton
                        aria-label="close"
                        color="inherit"
                        size="small"
                        onClick={() => {
                            dispatch(removeHint(id))
                        }}
                    >
                        <CloseIcon fontSize="inherit" />
                    </IconButton>
                }
            >
                {text}
            </Alert>
        </Collapse>
    );
}
