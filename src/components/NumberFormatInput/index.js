import React from 'react';
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';
import TextField from '@material-ui/core/TextField';

function NumberFormatCustom(props) {
    const { inputRef, onChange, ...other } = props;

    return (
        <NumberFormat
            {...other}
            getInputRef={inputRef}
            onValueChange={(values) => {
                onChange({
                    target: {
                        name: props.name,
                        value: values.value,
                    },
                });
            }}
            allowNegative={false}
            decimalSeparator="."
            decimalScale={2}
            fixedDecimalScale={true}
            thousandSeparator
            isNumericString
        />
    );
}

NumberFormatCustom.propTypes = {
    inputRef: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
};

export default function FormattedInput(props) { 
    const [value, setValue] = React.useState(null);
    React.useEffect(() => {
        setValue(props.value);
    }, [props.value])

    const handleChange = (event) => {
        setValue(event.target.value);
    };

    return (
        <div>
            <TextField
                value={value}
                onChange={handleChange}
                name="numberformat"
                id="formatted-numberformat-input"
                {...props}
                InputProps={{
                    inputComponent: NumberFormatCustom,
                }}
            />
        </div>
    );
}
