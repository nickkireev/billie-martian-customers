import { render, screen } from './utils/test-utils';
import App from './App';

test('renders main container', () => {
  const { container } = render(<App />);

  const getWrapperByClass = container.querySelector('.MuiContainer-root');
  expect(getWrapperByClass).toBeInTheDocument();

  const table = container.querySelector('table');
  expect(table).toBeInTheDocument();
});
