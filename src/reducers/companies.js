const company = (state, action) => {
    switch (action.type) {
        case 'ADD_COMPANY':
            // add new data
            return {
                ...action.data
            }
        case 'UPDATE_COMPANY_SUCCESS':
            // find company by id and change it
            if (state.id !== action.data.id) {
                return state;
            }

            //replace existed data with received
            return action.data;
        default:
            return state
    }
}

const companies = (state = [], action) => {
    switch (action.type) {
        case 'FETCH_COMPANIES_SUCCESS':
            return action.data;
        case 'ADD_COMPANY':
            return [
                ...state,
                company(undefined, action)
            ];
        case 'UPDATE_COMPANY_SUCCESS':
            return state.map(st => {
                return company(st, action)
            });
        default:
            return state;
    }
}

export default companies;
