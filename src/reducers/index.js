import { combineReducers } from 'redux';
import companies from './companies';
import hints from './hints';

const mainApp = combineReducers({
    hints,
    companies
})

export default mainApp;
