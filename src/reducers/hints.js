let hintCounter = 0;

const hints = (state = [], action) => {
    switch (action.type) {
        case 'ADD_HINT':
            hintCounter++;
            return [
                ...state,
                {
                    id: hintCounter,
                    severity: action.severity,
                    text: action.text,
                    open: true
                }
            ]
        case 'REMOVE_HINT':
            return state.filter(hint => (hint.id !== action.id));
        default:
            return state
    }
}

export default hints;
