import './App.css';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Companies from './components/Companies';

const useStyles = makeStyles({
  container: {
    marginTop: 65,
  },
});

function App() {
  const classes = useStyles();
  return (
    <div className="App">
      <CssBaseline />
      <Container maxWidth="xl" className={classes.container} >
        <Companies />
      </Container>
    </div>
  );
}

export default App;
